#ifndef UTILITY_H
#define UTILITY_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <windows.h>
#include <tchar.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

// Vectors
typedef struct
{
    GLfloat x, y;
} vec2;

typedef struct
{
    GLfloat x, y, z;
} vec3;

typedef struct
{
    GLfloat x, y, z, w;
} vec4;

// Double Vectors
typedef struct
{
    GLdouble x, y;
} dvec2;

typedef struct
{
    GLdouble x, y, z;
} dvec3;

typedef struct
{
    GLdouble x, y, z, w;
} dvec4;

// Matrices 
typedef struct
{
    vec2 x, y;
} mat2;

typedef struct
{
    vec3 x, y, z;
} mat3;

typedef struct
{
    vec4 x, y, z, w;
} mat4;

// Double Matrices
typedef struct
{
    dvec2 x, y;
} dmat2;

typedef struct
{
    dvec3 x, y, z;
} dmat3;

typedef struct
{
    dvec4 x, y, z, w;
} dmat4;



mat4 multiply_mat4(mat4*const A, mat4*const B);

dmat4 multiply_dmat4(dmat4*const A, dmat4*const B);

int identity(mat4* identity);

int rotate(mat4* rotation, GLfloat phi, GLfloat theta, GLfloat psi);

int translate(mat4* translation, GLfloat x, GLfloat y, GLfloat z);

void calculate_rotation(GLFWwindow* window, GLdouble* phi, GLdouble* theta, GLdouble* psi);

void calculate_translation(GLFWwindow* window, GLdouble* const phi, GLdouble* const theta, GLdouble* const psi,  GLdouble* x, GLdouble* y, GLdouble* z);

HANDLE init_watch_folder();

void watch_folder(HANDLE dwChangeHandle, void(*rebuild)(GLuint*, GLuint*), GLuint* compute_program, GLuint* locations);

GLdouble normal_distribution(GLdouble x, GLdouble mu, GLdouble sigma);

#endif //UTILITY_H
