#ifndef SHAPE_H
#define SHAPE_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "color.h"
#include "utility.h"

typedef struct
{
    mat4 transform;
    vec4 v0;
    vec4 v1;
    vec4 v2;
    GLdouble power;
    GLdouble padding;
    GLuint type;
    GLuint color_index;
    GLuint padding2;
    GLuint padding3;
} Shape;

void init_shape(Shape *shape);

#endif //SHAPE_H
