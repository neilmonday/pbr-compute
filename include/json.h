#ifndef JSON_H
#define JSON_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cassert>

#include <jsmn.h>

#include "shape.h"
#include "color.h"

#define SPHERE 0x1;
#define TRIANGLE 0x2;

float parse_float(int* i, jsmntok_t* const t, char* const buffer);
vec4 parse_vec4(int* i, jsmntok_t* const t, char* const buffer);
int parse_scene(char* filename, Shape** shapes, GLuint* shapes_count, Color** colors, GLuint* colors_count);

#endif // !JSON_H
