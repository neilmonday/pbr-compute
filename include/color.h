#ifndef COLOR_H
#define COLOR_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "utility.h"

#define WAVELENGTHS 76

const vec4 cie_color_match[WAVELENGTHS];

typedef struct
{
    //HACK std140 requires a stride of 16-bytes, so an array of doubles in C needs to have padding of 8-bytes between the doubles.
    double spectrum[WAVELENGTHS * 2];
} Color;

void init_color(Color *color);

#endif //COLOR_H
