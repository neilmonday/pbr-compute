#version 430 core

uniform sampler3D texture;

out vec4 fs_out;

void main()
{
    ivec2 position = ivec2(gl_FragCoord.xy);

    const float gamma = 2.2;
    vec4 hdrColor;
    for(int i = 0; i < 76; i++)
    {
        hdrColor += texelFetch(texture, ivec3(position.x, position.y, i), 0);
    }
    hdrColor/= 76.0;
    // luma based Reinhard op.,
    /*float luma = dot(hdrColor, vec3(0.2126, 0.7152, 0.0722));
    float toneMappedLuma = luma / (1. + luma);
    vec3 mapped = hdrColor * toneMappedLuma / luma;*/

    // reinhard tone mapping
    //vec3 mapped = hdrColor / (hdrColor + vec3(1.0));

    // Exposure tone mapping
    const float exposure = 0.000005;
    vec3 mapped = vec3(1.0) - exp(-hdrColor.xyz * exposure);

    // gamma correction
    mapped = pow(mapped, vec3(1.0 / gamma));
    fs_out = vec4(mapped, 1.0);
}
