#include "utility.h"

const GLdouble PI = 3.1415926535897932384626433832795;
const GLdouble E = 2.7182818284590452353602874713526;

mat4 multiply_mat4(mat4*const A, mat4*const B)
{
    mat4 result;

    result.x.x = A->x.x * B->x.x + A->x.y * B->y.x + A->x.z * B->z.x + A->x.w * B->w.x;
    result.x.y = A->x.x * B->x.y + A->x.y * B->y.y + A->x.z * B->z.y + A->x.w * B->w.y;
    result.x.z = A->x.x * B->x.z + A->x.y * B->y.z + A->x.z * B->z.z + A->x.w * B->w.z;
    result.x.w = A->x.x * B->x.w + A->x.y * B->y.w + A->x.z * B->z.w + A->x.w * B->w.w;

    result.y.x = A->y.x * B->x.x + A->y.y * B->y.x + A->y.z * B->z.x + A->y.w * B->w.x;
    result.y.y = A->y.x * B->x.y + A->y.y * B->y.y + A->y.z * B->z.y + A->y.w * B->w.y;
    result.y.z = A->y.x * B->x.z + A->y.y * B->y.z + A->y.z * B->z.z + A->y.w * B->w.z;
    result.y.w = A->y.x * B->x.w + A->y.y * B->y.w + A->y.z * B->z.w + A->y.w * B->w.w;

    result.z.x = A->z.x * B->x.x + A->z.y * B->y.x + A->z.z * B->z.x + A->z.w * B->w.x;
    result.z.y = A->z.x * B->x.y + A->z.y * B->y.y + A->z.z * B->z.y + A->z.w * B->w.y;
    result.z.z = A->z.x * B->x.z + A->z.y * B->y.z + A->z.z * B->z.z + A->z.w * B->w.z;
    result.z.w = A->z.x * B->x.w + A->z.y * B->y.w + A->z.z * B->z.w + A->z.w * B->w.w;

    result.w.x = A->w.x * B->x.x + A->w.y * B->y.x + A->w.z * B->z.x + A->w.w * B->w.x;
    result.w.y = A->w.x * B->x.y + A->w.y * B->y.y + A->w.z * B->z.y + A->w.w * B->w.y;
    result.w.z = A->w.x * B->x.z + A->w.y * B->y.z + A->w.z * B->z.z + A->w.w * B->w.z;
    result.w.w = A->w.x * B->x.w + A->w.y * B->y.w + A->w.z * B->z.w + A->w.w * B->w.w;

    return result;
}

dmat4 multiply_dmat4(dmat4*const A, dmat4*const B)
{
    dmat4 result;

    result.x.x = A->x.x * B->x.x + A->x.y * B->y.x + A->x.z * B->z.x + A->x.w * B->w.x;
    result.x.y = A->x.x * B->x.y + A->x.y * B->y.y + A->x.z * B->z.y + A->x.w * B->w.y;
    result.x.z = A->x.x * B->x.z + A->x.y * B->y.z + A->x.z * B->z.z + A->x.w * B->w.z;
    result.x.w = A->x.x * B->x.w + A->x.y * B->y.w + A->x.z * B->z.w + A->x.w * B->w.w;

    result.y.x = A->y.x * B->x.x + A->y.y * B->y.x + A->y.z * B->z.x + A->y.w * B->w.x;
    result.y.y = A->y.x * B->x.y + A->y.y * B->y.y + A->y.z * B->z.y + A->y.w * B->w.y;
    result.y.z = A->y.x * B->x.z + A->y.y * B->y.z + A->y.z * B->z.z + A->y.w * B->w.z;
    result.y.w = A->y.x * B->x.w + A->y.y * B->y.w + A->y.z * B->z.w + A->y.w * B->w.w;

    result.z.x = A->z.x * B->x.x + A->z.y * B->y.x + A->z.z * B->z.x + A->z.w * B->w.x;
    result.z.y = A->z.x * B->x.y + A->z.y * B->y.y + A->z.z * B->z.y + A->z.w * B->w.y;
    result.z.z = A->z.x * B->x.z + A->z.y * B->y.z + A->z.z * B->z.z + A->z.w * B->w.z;
    result.z.w = A->z.x * B->x.w + A->z.y * B->y.w + A->z.z * B->z.w + A->z.w * B->w.w;

    result.w.x = A->w.x * B->x.x + A->w.y * B->y.x + A->w.z * B->z.x + A->w.w * B->w.x;
    result.w.y = A->w.x * B->x.y + A->w.y * B->y.y + A->w.z * B->z.y + A->w.w * B->w.y;
    result.w.z = A->w.x * B->x.z + A->w.y * B->y.z + A->w.z * B->z.z + A->w.w * B->w.z;
    result.w.w = A->w.x * B->x.w + A->w.y * B->y.w + A->w.z * B->z.w + A->w.w * B->w.w;

    return result;
}

int identity(mat4* set_identity)
{
    mat4 identity;
    identity.x.x = 1.0f;
    identity.x.y = 0.0f;
    identity.x.z = 0.0f;
    identity.x.w = 0.0f;

    identity.y.x = 0.0f;
    identity.y.y = 1.0f;
    identity.y.z = 0.0f;
    identity.y.w = 0.0f;

    identity.z.x = 0.0f;
    identity.z.y = 0.0f;
    identity.z.z = 1.0f;
    identity.z.w = 0.0f;

    identity.w.x = 0.0f;
    identity.w.y = 0.0f;
    identity.w.z = 0.0f;
    identity.w.w = 1.0f;
    *set_identity = identity;
    return 0;
}

int rotate(mat4* rotation, GLfloat phi, GLfloat theta, GLfloat psi)
{
    mat4 rotation_phi;

    rotation_phi.x.x = 1.0f;
    rotation_phi.x.y = 0.0f;
    rotation_phi.x.z = 0.0f;
    rotation_phi.x.w = 0.0f;

    rotation_phi.y.x = 0.0f;
    rotation_phi.y.y = (GLfloat)cos(phi);
    rotation_phi.y.z = (GLfloat)sin(phi);
    rotation_phi.y.w = 0.0f;

    rotation_phi.z.x = 0.0f;
    rotation_phi.z.y = (GLfloat)-sin(phi);
    rotation_phi.z.z = (GLfloat)cos(phi);
    rotation_phi.z.w = 0.0f;

    rotation_phi.w.x = 0.0f;
    rotation_phi.w.y = 0.0f;
    rotation_phi.w.z = 0.0f;
    rotation_phi.w.w = 1.0f;

    mat4 rotation_theta;

    rotation_theta.x.x = (GLfloat)cos(theta);
    rotation_theta.x.y = 0.0f;
    rotation_theta.x.z = (GLfloat)-sin(theta);
    rotation_theta.x.w = 0.0f;

    rotation_theta.y.x = 0.0f;
    rotation_theta.y.y = 1.0f;
    rotation_theta.y.z = 0.0f;
    rotation_theta.y.w = 0.0f;

    rotation_theta.z.x = (GLfloat)sin(theta);
    rotation_theta.z.y = 0.0f;
    rotation_theta.z.z = (GLfloat)cos(theta);
    rotation_theta.z.w = 0.0f;

    rotation_theta.w.x = 0.0f;
    rotation_theta.w.y = 0.0f;
    rotation_theta.w.z = 0.0f;
    rotation_theta.w.w = 1.0f;

    mat4 combined_rotation;
    combined_rotation = multiply_mat4(&rotation_phi, &rotation_theta);
    *rotation = multiply_mat4(&combined_rotation, rotation);
    return 0;
}

int translate(mat4* translation, GLfloat x, GLfloat y, GLfloat z)
{
    mat4 translate;

    translate.x.x = 1.0f;
    translate.x.y = 0.0f;
    translate.x.z = 0.0f;
    translate.x.w = 0.0f;

    translate.y.x = 0.0f;
    translate.y.y = 1.0f;
    translate.y.z = 0.0f;
    translate.y.w = 0.0f;

    translate.z.x = 0.0f;
    translate.z.y = 0.0f;
    translate.z.z = 1.0f;
    translate.z.w = 0.0f;

    translate.w.x = x;
    translate.w.y = y;
    translate.w.z = z;
    translate.w.w = 1.0f;

    *translation = multiply_mat4(&translate, translation);
    return 0;
}

void calculate_rotation(GLFWwindow* window, GLdouble* phi, GLdouble* theta, GLdouble* psi)
{
    static dvec2 initial_cursor = { 0.0, 0.0 };
    static dvec2 cursor = { 0.0, 0.0 };
    static dvec2 rotation = { 0.0, 0.0 };

    static int previous_mouse_state = 0;
    int mouse_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (mouse_state != previous_mouse_state)
    {
        if (mouse_state == GLFW_PRESS)
        {
            glfwGetCursorPos(window, &initial_cursor.x, &initial_cursor.y);

            /*printf("*****************\n");
            printf("Mouse Pressed: %f, %f\n", initial_cursor.x, initial_cursor.y);
            printf("*****************\n");*/
        }
    }
    if (mouse_state == GLFW_PRESS)
    {
        glfwGetCursorPos(window, &cursor.x, &cursor.y);

        *phi = (initial_cursor.y - cursor.y + rotation.y) / 200.0;
        *theta = (initial_cursor.x - cursor.x + rotation.x) / 200.0;

        /*printf("Mouse Down:\n");

        printf("    Initial: %f, %f\n", initial_cursor.x, initial_cursor.y);
        printf("    Cursor : %f, %f\n", cursor.x, cursor.y);
        printf("  Rotation : %f, %f\n", rotation.x, rotation.y);
        printf("Phi Theta: %f, %f\n", *phi, *theta);*/
    }
    if (mouse_state != previous_mouse_state)
    {
        if (mouse_state == GLFW_RELEASE)
        {
            rotation.x += (initial_cursor.x - cursor.x);
            rotation.y += (initial_cursor.y - cursor.y);

            /*printf("*****************\n");
            printf("Mouse Up:\n");
            printf("    Initial: %f, %f\n", initial_cursor.x, initial_cursor.y);
            printf("    Cursor : %f, %f\n", cursor.x, cursor.y);
            printf("  Rotation : %f, %f\n", rotation.x, rotation.y);
            printf("*****************\n");*/
        }
    }

    previous_mouse_state = mouse_state;
}


void calculate_translation(GLFWwindow* window, GLdouble* const phi, GLdouble* const theta, GLdouble* const psi, GLdouble* x, GLdouble* y, GLdouble* z)
{
    GLdouble sensitivity = 20.0;
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        *x -= sensitivity * sin(*theta) * cos(*phi);
        *y -= sensitivity * 1           *-sin(*phi);
        *z -= sensitivity * cos(*theta) * cos(*phi);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        *x -= sensitivity * cos(*theta);
        //*y -=;
        *z -= sensitivity * -sin(*theta);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        *x += sensitivity * sin(*theta) * cos(*phi);
        *y += sensitivity * 1 * -sin(*phi);
        *z += sensitivity * cos(*theta) * cos(*phi);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        *x += sensitivity * cos(*theta);
        //*y += ;
        *z += sensitivity * -sin(*theta);
    }
}

HANDLE init_watch_folder( LPTSTR lpDir)
{
    // Wait for notification.
    HANDLE dwChangeHandle;
    TCHAR lpDrive[4];
    TCHAR lpFile[_MAX_FNAME];
    TCHAR lpExt[_MAX_EXT];

    _tsplitpath_s(lpDir, lpDrive, 4, NULL, 0, lpFile, _MAX_FNAME, lpExt, _MAX_EXT);

    lpDrive[2] = (TCHAR)'\\';
    lpDrive[3] = (TCHAR)'\0';

    // Watch the directory for file creation and deletion. 

    dwChangeHandle = FindFirstChangeNotification(
        lpDir,                         // directory to watch 
        TRUE,                         // do not watch subtree 
        FILE_NOTIFY_CHANGE_LAST_WRITE); // watch file name changes 

    if (dwChangeHandle == INVALID_HANDLE_VALUE)
    {
        printf("\n ERROR: FindFirstChangeNotification function failed.\n");
        ExitProcess(GetLastError());
    }

    // Make a final validation check on our handles.

    if (dwChangeHandle == NULL)
    {
        printf("\n ERROR: Unexpected NULL from FindFirstChangeNotification.\n");
        ExitProcess(GetLastError());
    }

    return dwChangeHandle;
}

void watch_folder(HANDLE dwChangeHandle, void(*rebuild)(GLuint*, GLuint*), GLuint* compute_program, GLuint* locations)
{
    DWORD dwWaitStatus = WaitForMultipleObjects(1, &dwChangeHandle, FALSE, 1);
    switch (dwWaitStatus)
    {
    case WAIT_OBJECT_0:
        printf("file has changed.\n");
        rebuild(compute_program, locations);
        if (FindNextChangeNotification(dwChangeHandle) == FALSE)
        {
            printf("\n ERROR: FindNextChangeNotification function failed.\n");
            ExitProcess(GetLastError());
        }
        break;
    case WAIT_TIMEOUT:
        //printf("No changes in the timeout period.\n");
        break;

    default:
        printf("ERROR: Unhandled dwWaitStatus.\n");
        ExitProcess(GetLastError());
        break;
    }
}

GLdouble normal_distribution(GLdouble x, GLdouble mu, GLdouble sigma)
{
    GLdouble coeffitient = 1 / sqrt(2 * PI*sigma*sigma);
    GLdouble exponent = -((x - mu) * (x - mu)) / (2 * sigma*sigma);
    return coeffitient * pow(E, exponent);
}
