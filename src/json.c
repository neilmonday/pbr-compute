#include "json.h"

static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
    if (tok->type == JSMN_STRING && (int)strlen(s) == tok->end - tok->start &&
        strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
        return 0;
    }
    return -1;
}

float parse_float(int* i, jsmntok_t* const t, char* const buffer)
{
    size_t size = t[*i].end - t[*i].start;
    //malloc and free so frequently is dumb.
    char* float_string = (char*)malloc(size+1);
    memcpy(float_string, buffer + t[*i].start, size);
    float_string[size] = '\0';
    float new_float = (float)atof(float_string);
    free(float_string);
    return new_float;
}

int parse_int(int* i, jsmntok_t* const t, char* const buffer)
{
    size_t size = t[*i].end - t[*i].start;
    //malloc and free so frequently is dumb.
    char* int_string = (char*)malloc(size + 1);
    memcpy(int_string, buffer + t[*i].start, size);
    int_string[size] = '\0';
    float new_int = (float)atoi(int_string);
    free(int_string);
    return (int)new_int;
}

vec4 parse_vec4(int* i, jsmntok_t* const t, char* const buffer)
{
    vec4 result;
    int size = t[*i].size;
    int j;
    (*i)++;//consume the object and get to the string position/color/normal
    for (j = 0; j < size; j++)
    {
        if (jsoneq(buffer, &t[*i], "x") == 0)
        {
            (*i)++; //consume the string
            result.x = parse_float(i, t, buffer);
            (*i)++; //consume the float
        }
        else if (jsoneq(buffer, &t[*i], "y") == 0)
        {
            (*i)++; //consume the string
            result.y = parse_float(i, t, buffer);
            (*i)++; //consume the float
        }
        else if (jsoneq(buffer, &t[*i], "z") == 0)
        {
            (*i)++; //consume the string
            result.z = parse_float(i, t, buffer);
            (*i)++; //consume the float
        }
        else if (jsoneq(buffer, &t[*i], "w") == 0)
        {
            (*i)++; //consume the string
            result.w = parse_float(i, t, buffer);
            (*i)++; //consume the float
        }
        else
        {
            (*i)++; //this will not work properly unless all items in the array are key/value.
        }
    }
    return result;
}

int parse_spectrum(int* i, jsmntok_t* const t, char* const buffer, double result[])
{
    if (t[*i].type == JSMN_ARRAY)
    {
        int array_size = t[*i].size;
        (*i)++;//consume the array now that we have it's size
        int k;
        for (k = 0; k < array_size; k++) //every object in spectrum.
        {
            int object_size = t[*i].size;
            (*i)++;//consume the object {"wavelength":400.0, "power":0.0}, now that we have its size
            int l;
            int index = -1;
            for (l = 0; l < object_size; l++)
            {
                if (jsoneq(buffer, &t[*i], "wavelength") == 0)
                {
                    (*i)++; //consume the string wavelength
                    index = (int)round((parse_float(i, t, buffer) - 400.0) / 4.0);
                    (*i)++; //consume the float
                }
                else if (jsoneq(buffer, &t[*i], "power") == 0)
                {
                    assert(index > -1);
                    (*i)++; //consume the string power
                    //HACK std140 requires a stride of 16-bytes, so an array of doubles in C needs to have padding of 8-bytes between the doubles.
                    result[index*2] = parse_float(i, t, buffer);
                    (*i)++; //consume the float
                }
            }
        }
    }
    return 0;
}

int parse_scene(char* filename, Shape** shapes, GLuint* shapes_count, Color** colors, GLuint* colors_count)
{
    long lSize;
    char * buffer;
    size_t result;

    FILE * pFile = fopen(filename, "rb");

    if (pFile == NULL) { fputs("File error", stderr); exit(1); }

    // obtain file size:
    fseek(pFile, 0, SEEK_END);
    lSize = ftell(pFile);
    rewind(pFile);

    // allocate memory to contain the whole file:
    buffer = (char*)malloc(sizeof(char)*lSize + 1);
    buffer[lSize] = '\0';
    if (buffer == NULL) { fputs("Memory error", stderr); exit(2); }

    // copy the file into the buffer:
    result = fread(buffer, 1, lSize, pFile);
    if (result != lSize) { fputs("Reading error", stderr); exit(3); }

    jsmn_parser parser;
    jsmntok_t t[16384];

    int i = 0;
    int j;

    int size;

    jsmn_init(&parser);
    int r = jsmn_parse(&parser, buffer, strlen(buffer), t, sizeof(t)/sizeof(t[0]));

    i++; //consume the first object
    while(i<r)
    {
        if ((t[i].type == JSMN_STRING) && (jsoneq(buffer, &t[i], "colors") == 0))
        {
            i++; //consume the colors string.
            if (t[i].type == JSMN_ARRAY)
            {
                *colors_count = t[i].size;
                *colors = (Color*)malloc(sizeof(Color) * (*colors_count));

                i++;//consume the array now that we have it's size
                int color;
                for (color = 0; color < *colors_count; color++) //every object in spectrum.
                {
                    init_color(&((*colors)[color]));
                    int object_size = t[i].size;
                    i++;//consume the object {"wavelength":400.0, "power":0.0}, now that we have its size
                    int l;
                    for (l = 0; l < object_size; l++)
                    {
                        if (jsoneq(buffer, &t[i], "name") == 0)
                        {
                            i++; // consume the string "name"
                            i++; // consume the name
                        }
                        else if (jsoneq(buffer, &t[i], "type") == 0)
                        {
                            i++; // consume the string "name"
                            i++; // consume the name
                        }
                        else if (jsoneq(buffer, &t[i], "spectrum") == 0)
                        {
                            i++;//consume the string spectrum
                            parse_spectrum(&i, t, buffer, (*colors)[color].spectrum);
                        }
                    }
                }
            }
        }
        else if ((t[i].type == JSMN_STRING) && (jsoneq(buffer, &t[i], "shapes") == 0))
        {
            i++; //consume the "shapes" string.
            *shapes_count = t[i].size;
            *shapes = (Shape*)malloc(sizeof(Shape) * (*shapes_count));
            int shape = 0;
            i++; //consume the object
            for (shape = 0; shape < *shapes_count; shape++)
            {
                if ((t[i].type == JSMN_STRING) && (jsoneq(buffer, &t[i], "sphere") == 0))
                {
                    i++; //consume the string and get to the object.
                    init_shape(&((*shapes)[shape]));
                    (*shapes)[shape].type = SPHERE;

                    size = t[i].size;
                    i++;//consume the object and get to the string position/color/normal
                    for (j = 0; j < size; j++)
                    {
                        if (jsoneq(buffer, &t[i], "position") == 0)
                        {
                            i++; //consume the string position
                            vec4 position = parse_vec4(&i, t, buffer);
                            translate(&(*shapes)[shape].transform, position.x, position.y, position.z);
                        }
                        else if (jsoneq(buffer, &t[i], "radius") == 0)
                        {
                            i++; //consume the string radius
                            (*shapes)[shape].v0.x = parse_float(&i, t, buffer);
                            i++; //consume the float
                        }
                        else if (jsoneq(buffer, &t[i], "power") == 0)
                        {
                            i++; //consume the string power
                                 //only grabbing the first float of the vec4
                            (*shapes)[shape].power = parse_vec4(&i, t, buffer).x;
                        }
                        else if (jsoneq(buffer, &t[i], "color") == 0)
                        {
                            i++; //consume the string color
                            (*shapes)[shape].color_index = (GLuint)parse_float(&i, t, buffer);
                            i++; // consume the int
                        }
                        else
                        {
                            i++; //this will not work properly unless all items in the array are key/value.
                        }
                    }
                }
                else if ((t[i].type == JSMN_STRING) && (jsoneq(buffer, &t[i], "triangle") == 0))
                {
                    i++; //consume the string and get to the object.

                    init_shape(&((*shapes)[shape]));
                    (*shapes)[shape].type = TRIANGLE;

                    size = t[i].size;
                    i++;//consume the object and get to the string position/color/normal
                    for (j = 0; j < size; j++)
                    {
                        if (jsoneq(buffer, &t[i], "v0") == 0)
                        {
                            i++; //consume the string v0
                            (*shapes)[shape].v0 = parse_vec4(&i, t, buffer);
                        }
                        else if (jsoneq(buffer, &t[i], "v1") == 0)
                        {
                            i++; //consume the string v1
                            (*shapes)[shape].v1 = parse_vec4(&i, t, buffer);
                        }
                        else if (jsoneq(buffer, &t[i], "v2") == 0)
                        {
                            i++; //consume the string v2
                            (*shapes)[shape].v2 = parse_vec4(&i, t, buffer);
                        }
                        else if (jsoneq(buffer, &t[i], "name") == 0)
                        {
                            i++; // consume the string "name"
                            i++; // consume the name
                        }
                        else if (jsoneq(buffer, &t[i], "color") == 0)
                        {
                            i++; //consume the string color
                            (*shapes)[shape].color_index = (GLuint)parse_float(&i, t, buffer);
                            i++; // consume the int
                        }
                        else if (jsoneq(buffer, &t[i], "power") == 0)
                        {
                            i++; //consume the string power
                            //only grabbing the first float of the vec4
                            (*shapes)[shape].power = parse_vec4(&i, t, buffer).x;
                        }
                        else
                        {
                            i++;
                        }
                    }
                }
            }
        }
    }
    return 0;
}
