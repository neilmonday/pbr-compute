#include "shape.h"

void init_shape(Shape *shape)
{
    identity(&(shape->transform));
    shape->v0.x = 0.0f;
    shape->v0.y = 0.0f;
    shape->v0.z = 0.0f;
    shape->v0.w = 1.0f;
    shape->v1.x = 0.0f;
    shape->v1.y = 0.0f;
    shape->v1.z = 0.0f;
    shape->v1.w = 1.0f;
    shape->v2.x = 0.0f;
    shape->v2.y = 0.0f;
    shape->v2.z = 0.0f;
    shape->v2.w = 1.0f;
    shape->power = 0.0;
    shape->type = 0;
    shape->color_index = 5;
}
