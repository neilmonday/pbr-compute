#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <windows.h>
#include <tchar.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "utility.h"
#include "program.h"
#include "texture.h"
#include "shape.h"
#include "color.h"
#include "json.h"

Texture image;

void window_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
    image.width = width;
    image.height = height;
    UpdateImage(image);
}

void main(void)
{
    Shape* shapes;
    Color* colors;
    GLuint shapes_count;
    GLuint colors_count;
    //char* filename = "../../scenes/example.json";
    char* filename = "../../scenes/cornell.json";
    //char* filename = "../../scenes/planets.json";
    parse_scene(filename, &shapes, &shapes_count, &colors, &colors_count);
    GLfloat seed = 0.0;

    GLFWwindow* window;
    // Initialise GLFW
    if (!glfwInit())
        return;

    int initialWidth = 512;
    int initialHeight = 512;

    // Create a fullscreen mode window and its OpenGL context
    //window = glfwCreateWindow(initialWidth, initialHeight, "PBR-Compute", glfwGetPrimaryMonitor(), NULL);
    // Create a windowed mode window and its OpenGL context 
    window = glfwCreateWindow(initialWidth, initialHeight, "PBR-Compute", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        return;
    }

    glfwSetWindowSizeCallback(window, &window_size_callback);

    // Make the window's context current
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = GL_TRUE;
    GLenum err = glewInit();

    if (GLEW_OK != err)
        return;

    glActiveTexture(GL_TEXTURE0 + 0);
    image = CreateImage(initialWidth, initialHeight, WAVELENGTHS);
    glBindImageTexture(0, image.texID, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
    glBindTexture(GL_TEXTURE_3D, image.texID);

    // Initialize 
    GLuint graphics_program = 0;
    build_graphics_program(&graphics_program);

    GLuint compute_program = 0;
    GLuint locations[64];
    build_compute_program(&compute_program, locations);

    //create a VAO
    GLuint vertex_array_object;
    err = glGetError(); assert(!err);
    glGenVertexArrays(1, &vertex_array_object);
    glBindVertexArray(vertex_array_object);

    GLfloat vertices[] = {
        -1.0f, -1.0f,
         1.0f, -1.0f,
        -1.0f,  1.0f,
         1.0f, -1.0f,
         1.0f,  1.0f,
        -1.0f,  1.0f
    };

    int width = 0, height = 0, previous_width = 0, previous_height = 0;
    glfwGetWindowSize(window, &width, &height);

    //This must match the local_size_ from the compute shader
    GLuint local_size_x = 2;
    GLuint local_size_y = 4;
    GLuint local_size_z = WAVELENGTHS;
    GLuint num_work_groups_x = width/ local_size_x;
    GLuint num_work_groups_y = height / local_size_y;
    GLuint num_work_groups_z = 1;

    err = glGetError(); assert(!err);
    glUniform1i(locations[IMAGE_LOCATION], 0);
    err = glGetError(); assert(!err);
    glUniform1ui(locations[SHAPES_COUNT_LOCATION], shapes_count);
    err = glGetError(); assert(!err);

    //create a VBO
    GLuint vertex_buffer_object;
    glGenBuffers(1, &vertex_buffer_object);
    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer_object);
    glBufferData(GL_ARRAY_BUFFER, 6 * 2 * 4, vertices, GL_STATIC_DRAW);
    err = glGetError(); assert(!err);

    GLint position = glGetAttribLocation(graphics_program, "position");
    glVertexAttribPointer(position, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(position);
    err = glGetError(); assert(!err);

    GLuint shapes_uniform_buffer_object;
    glGenBuffers(1, &shapes_uniform_buffer_object);
    glBindBuffer(GL_UNIFORM_BUFFER, shapes_uniform_buffer_object);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(Shape) * shapes_count, shapes, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    //In the shader, I have the "binding=1" for the shapes
    glBindBufferBase(GL_UNIFORM_BUFFER, 1, shapes_uniform_buffer_object);
    err = glGetError(); assert(!err);

    GLuint cie_color_match_uniform_buffer_object;
    glGenBuffers(1, &cie_color_match_uniform_buffer_object);
    glBindBuffer(GL_UNIFORM_BUFFER, cie_color_match_uniform_buffer_object);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(vec4) * WAVELENGTHS, cie_color_match, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    //In the shader, I have the "binding=2" for the colors
    glBindBufferBase(GL_UNIFORM_BUFFER, 2, cie_color_match_uniform_buffer_object);
    err = glGetError(); assert(!err);

    GLuint reflection_uniform_buffer_object;
    glGenBuffers(1, &reflection_uniform_buffer_object);
    glBindBuffer(GL_UNIFORM_BUFFER, reflection_uniform_buffer_object);
    //HACK std140 requires a stride of 16-bytes, so an array of doubles in C needs to have padding of 8-bytes between the doubles.
    glBufferData(GL_UNIFORM_BUFFER, sizeof(double) * WAVELENGTHS * 4 * 2, colors, GL_STATIC_DRAW);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);

    //In the shader, I have the "binding=3" for the reflections
    glBindBufferBase(GL_UNIFORM_BUFFER, 3, reflection_uniform_buffer_object);
    err = glGetError(); assert(!err);

    //GLFW state tracking
    int previous_mouse_state = 0;
    GLdouble phi = 0, theta = 0, psi = 0;
    GLdouble camera_x = 0, camera_y = 0, camera_z = 0; //either 2048 x 2048 and 1350.0 here or 512 x 512 and 3000.0 here

    //camera_z should be 3100.0 if the window is 512x512
    //camera_z should be 1950.0 if the window is 1024x1024
    //camera_z should be 1350.0 if the window is 2048x2048

    //What we use to transform the scene based on the GLFW inputs above.
    mat4 global_transform;

    HANDLE dwChangeHandle = init_watch_folder("../../shaders/");

    unsigned int frame_count = 0;

    // Loop until the user closes the window 
    while (!glfwWindowShouldClose(window))
    {
        glUseProgram(compute_program);
        glfwGetWindowSize(window, &width, &height);

        if (previous_height != height || previous_width != width)
        {
            previous_width = width;
            previous_height = height;
        }

        identity(&global_transform);
        translate(&global_transform, 278.0f, 273.0f, -800.0);
        rotate(&global_transform, 0.0f, 3.14f, 0.0f);

        err = glGetError(); assert(!err);

        const GLfloat color[] = { 0.5f, 0.5f, 0.0f, 1.0f };
        glClearBufferfv(GL_COLOR, 0, color);

        glFinish();

        calculate_rotation(window, &phi, &theta, &psi);
        calculate_translation(window, &phi, &theta, &psi, &camera_x, &camera_y, &camera_z);
        //Make sure you keep track of the order in which the transformations occur.
        translate(&global_transform, (GLfloat)camera_x, (GLfloat)camera_y, (GLfloat)camera_z);
        rotate(&global_transform, (GLfloat)phi, (GLfloat)theta, (GLfloat)psi);

        seed += (float)rand() / (float)RAND_MAX;
        glUniform1f(locations[SEED_LOCATION], seed);
        glUniform1ui(locations[SHAPES_COUNT_LOCATION], shapes_count);
        glUniformMatrix4fv(locations[TRANSFORM_LOCATION], 1, GL_FALSE, (GLfloat*)&global_transform);
        glUniform1ui(locations[FRAME_COUNT_LOCATION], frame_count);

        //Render to the image
        err = glGetError(); assert(!err);
        glDispatchCompute(num_work_groups_x, num_work_groups_y, num_work_groups_z);
                    
        err = glGetError(); assert(!err);
                
        //Draw the image to the screen
        glUseProgram(graphics_program);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glUseProgram(compute_program);
        glfwSwapBuffers(window);
        glfwPollEvents();

        watch_folder(dwChangeHandle, &build_compute_program, &compute_program, locations);

        frame_count++;
    }

    glDeleteVertexArrays(1, &vertex_array_object);
    glfwTerminate();
    return;
}
